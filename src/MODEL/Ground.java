package MODEL;

import java.awt.Color;

import javax.swing.JLabel;

public class Ground {

	private int xmax=50;
	private int ymax=50;
	private Color colorDef=Color.YELLOW;
	private Case[][] board;
	/**
	 * @param xmax
	 * @param ymax
	 * @param board
	 */
	public Ground(int xmax, int ymax) {
		super();
		this.xmax = xmax;
		this.ymax = ymax;
		board=new Case[xmax][ymax];
		for(int i=0;i<xmax;i++) {
			for(int j=0;j<ymax;j++) {
				board[i][j]=new Case(i,j,colorDef," ");
			}
	}
	}
	/**
	 * @return the xmax
	 */
	public int getXmax() {
		return xmax;
	}
	/**
	 * @param xmax the xmax to set
	 */
	public void setXmax(int xmax) {
		this.xmax = xmax;
	}
	/**
	 * @return the ymax
	 */
	public int getYmax() {
		return ymax;
	}
	/**
	 * @param ymax the ymax to set
	 */
	public void setYmax(int ymax) {
		this.ymax = ymax;
	}
	/**
	 * @return the board
	 */
	public Case[][] getBoard() {
		return board;
	}
	/**
	 * @param board the board to set
	 */
	public void setBoard(Case[][] board) {
		this.board = board;
	}
	/**
	 * @return the colorDef
	 */
	public Color getColorDef() {
		return colorDef;
	}
	/**
	 * @param colorDef the colorDef to set
	 */
	public void setColorDef(Color colorDef) {
		this.colorDef = colorDef;
	}
	
	
	
	
}
