package MODEL;

import java.awt.Color;
import java.util.ArrayList;

public class Snake {
	private Case head;
	private ArrayList<Case> body=new ArrayList<>();
	private Color colorBody;
	private String direction;
	/**
	 * @param head
	 * @param body
	 * @param color
	 * @param direction
	 */
	public Snake() {
		super();
		this.head = new Case(0,0,Color.DARK_GRAY," ");
		this.body.add(head);
		this.colorBody = Color.BLUE;
		this.direction = "RIGHT";
		grow();
		grow();
		
	}
	/**
	 * @return the head
	 */
	public Case getHead() {
		return head;
	}
	/**
	 * @param head the head to set
	 */
	public void setHead(Case head) {
		this.head = head;
	}
	/**
	 * @return the body
	 */
	public ArrayList<Case> getBody() {
		return body;
	}
	/**
	 * @param body the body to set
	 */
	public void setBody(ArrayList<Case> body) {
		this.body = body;
	}
	/**
	 * @return the color
	 */
	public Color getColor() {
		return colorBody;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(Color color) {
		this.colorBody = color;
	}
	/**
	 * @return the direction
	 */
	public String getDirection() {
		return direction;
	}
	/**
	 * @param direction the direction to set
	 */
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
	public void grow() {
		Case cases=new Case(0,0,colorBody," ");
		body.add(cases);
	}
	
	/*public void updatePos(int x,int y) {
		head.setX(x);
		head.setY(y);		
	}*/
	
}
