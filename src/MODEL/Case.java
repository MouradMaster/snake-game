package MODEL;

import java.awt.Color;

import javax.swing.JLabel;

public class Case {

	private int x;
	private int y;
	private Color color;
	private String symbol;
	/**
	 * @param x
	 * @param y
	 * @param color
	 * @param nature

	 */
	public Case(int x, int y, Color color, String symbol) {
		super();
		this.x = x;
		this.y = y;
		this.color = color;
		this.symbol = symbol;
	}
	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}
	/**
	 * @param symbol the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
	/**
	 * @param x
	 * @param y
	 * @param color
	 * @param name
	 
	 */
	public Case(int x, int y) {
		super();
		this.x = x;
		this.y = y;
		this.color = Color.YELLOW;
		this.symbol = " ";
	}
	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}
	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}
	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}
	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(Color color) {
		this.color = color;
	}
	
	public boolean isEmpty() {
		if(color==Color.YELLOW)
			return true;
		else
			return false;
	}
	
}
