package GUI;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import MODEL.*;

public class Gaming extends Thread {

	private boolean running=false;
	private boolean moving=false;
	private boolean eating=false;
	private boolean dead=false;
	
	private int delay=100;
	private Game game;
	private ArrayList<Case>body;
	/**
	 * @param game
	 */
	public Gaming(Game game) {
		super();
		body=game.getSnake().getBody();
		this.game = game;
		switch(game.getLevelSpeed()) {
		case 1 : delay = 150;break;
		case 2 : delay = 100;break;
		case 3 : delay = 50;break;
		case 4 : delay = 20;break;
		}
		
	}
	  public void run(){
		  running=true;	
		  moving=true;
		 game.addLevel();
		 repaint();
		  randomSnake();
		  suspend();
		  randoMeat();
		  while(running) {
		  while(!dead) 
		  move();	
		  running=false;
		  EndGame end=new EndGame();
		  end.frame.setVisible(true);
		  UpdateFile();
		  while(!end.isPress()) {
			  
		  }
			 
		  switch(end.getEnd()) {
		  case -1 :
			  
			  game.frmSnake.dispose();
			  end.frame.dispose();
			  Welcome welcome=new Welcome();
			  welcome.frame.setVisible(true);
			  break; 
		  
		  case 0 :
			  
			  game.frmSnake.dispose();	 
			  end.frame.dispose();	
			  Color[] color=new Color[3];
			  color[0]=game.getGround().getColorDef();
			  color[1]=game.getSnake().getColor();
			  color[2]=game.getMeat().getColor();
			  Game game2 = new Game(game.getLevel(),game.getLevelSpeed(),color);
			  game2.frmSnake.setVisible(true);
			  
						  break;
		  case 1 :
			  
			  game.frmSnake.dispose();			 
			  end.frame.dispose();
			  Color[] color2=new Color[3];
			  color2[0]=game.getGround().getColorDef();
			  color2[1]=game.getSnake().getColor();
			  color2[2]=game.getMeat().getColor();
			  if(game.getLevel()==5) {
				  Game game3 = new Game(1,game.getLevelSpeed(),color2);
				  game3.frmSnake.setVisible(true);
			  }
			  else {
			Game game3 = new Game(game.getLevel()+1,game.getLevelSpeed(),color2);
			  game3.frmSnake.setVisible(true);
			  }
			  
						  break;
		  }
		  
		  }
	  }
	
	public void move() {
		int x=game.getSnake().getHead().getX();
		int y=game.getSnake().getHead().getY();
		int xmax=game.getGround().getXmax();
		int ymax=game.getGround().getYmax();
		
		switch(game.getSnake().getDirection()) {
		  case "RIGHT" :	
			light(x,y);
			game.getSnake().getHead().setY(y+1);
			if(y==ymax-1) 
			game.getSnake().getHead().setY(0);;
			break;
		  
		  case "DOWN" :	
			 light(x,y);
			 game.getSnake().getHead().setX(x+1);
	 	   if(x==xmax-1) 
	 		  game.getSnake().getHead().setX(0);
			break;
		  case "LEFT" :	
			 light(x,y);
			 game.getSnake().getHead().setY(y-1);;					
			  if(y==0) 
			 game.getSnake().getHead().setY(ymax-1);;
					break;
		  case "UP" :	
			  light(x,y);
				 game.getSnake().getHead().setX(x-1);;					
				  if(x==0) 
				 game.getSnake().getHead().setX(xmax-1);;				
					break;

		  }
		
		
		for(int i=1;i<body.size();i++) {
			int p=body.get(i).getX();
			int q=body.get(i).getY();
			body.get(i).setX(x);
			body.get(i).setY(y);
			x=p;
			y=q;
		}
		
	}
	
	public void light(int x,int y) {
		Color color=game.getSnake().getColor();
		Color colorHead=game.getSnake().getHead().getColor();
		
		JLabel[][] board=game.getBoard();
		
		try {
			board[x][y].setBackground(colorHead);
			if(x==game.getMeat().getX() && y==game.getMeat().getY())
				{
				this.setEating(true);
				game.ScoreUp(game.getLevelSpeed()*game.getLevelSpeed());
				delay-=1;
				game.getScoreLabel().setText("SCORE : "+ game.getScore());
				if(game.getScore()>game.getBestScore())
					game.setBestScore(game.getScore());
				game.getBestScoreLabel().setText("BEST SCORE : "+ game.getBestScore());
				}
			for(int i=1;i<body.size();i++) {
				if(x==body.get(i).getX() && y==body.get(i).getY()) {
					this.setDead(true);
				}
			}
		
			for(int i=0;i<game.getBlock().size();i++) {
				if(x==game.getBlock().get(i).getX() && y==game.getBlock().get(i).getY()) {
					this.setDead(true);
				}
			}
			
			for(int i=1;i<body.size();i++) {
				int p=body.get(i).getX();
				int q=body.get(i).getY();
				board[p][q].setBackground(color);
			}
			sleep(delay);
			repaint();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void repaint() {
		Color colorDef=game.getGround().getColorDef();
		JLabel[][] board=game.getBoard();
		int xmax=game.getGround().getXmax();
		int ymax=game.getGround().getYmax();
		for(int i=0;i<xmax;i++)
			for(int j=0;j<ymax;j++) {
				board[i][j].setBackground(colorDef);
			}
		
	for(int i=0;i<game.getBlock().size();i++) {
		int xb=game.getBlock().get(i).getX();
		int yb=game.getBlock().get(i).getY();
		board[xb][yb].setBackground(game.getBlock().get(i).getColor());
	}
		
		
		if(!isEating()) {
			int xmeat=game.getMeat().getX();
			int ymeat=game.getMeat().getY();
			board[xmeat][ymeat].setBackground(game.getMeat().getColor());
		}
		else {
			randoMeat();
			game.getSnake().grow();
			setEating(false);
		}
		
		
		
	}
	
	/**
	 * @return the dead
	 */
	public boolean isDead() {
		return dead;
	}
	/**
	 * @param dead the dead to set
	 */
	public void setDead(boolean dead) {
		this.dead = dead;
	}
	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}
	/**
	 * @param running the running to set
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}
	/**
	 * @return the moving
	 */
	public boolean isMoving() {
		return moving;
	}
	/**
	 * @return the eating
	 */
	public boolean isEating() {
		return eating;
	}
	/**
	 * @param eating the eating to set
	 */
	public void setEating(boolean eating) {
		this.eating = eating;
	}
	/**
	 * @return the delay
	 */
	public int getDelay() {
		return delay;
	}
	/**
	 * @param delay the delay to set
	 */
	public void setDelay(int delay) {
		this.delay = delay;
	}
	
	/**
	 * @param moving the moving to set
	 */
	public void setMoving(boolean moving) {
		this.moving = moving;
	}
	
	public void randoMeat() {
		int nbX;
		int nbY;
		int xmax=game.getGround().getXmax();
		int ymax=game.getGround().getYmax();
		int min = 0;
		Case[][] car=game.getGround().getBoard();
		do {
			nbX = min + (int)(Math.random() * (xmax-1 - min));
			nbY = min + (int)(Math.random() * (ymax-1 - min));
		} while(!car[nbX][nbY].isEmpty());
		game.addMeat(nbX, nbY);
	}
	
	public void randomBlock(){
		int nbX;
		int nbY;
		int xmax=game.getGround().getXmax();
		int ymax=game.getGround().getYmax();
		int min = 0;
		Case[][] car=game.getGround().getBoard();
		do {
			nbX = min + (int)(Math.random() * (xmax-1 - min));
			nbY = min + (int)(Math.random() * (ymax-1 - min));
		} while(!car[nbX][nbY].isEmpty());
		game.addBlock(nbX, nbY);
	}
	
	public void randomSnake() {
		int nbX;
		int nbY;
		int min=2;
		int xmax=game.getGround().getXmax();
		int ymax=game.getGround().getYmax();
		int maxX=xmax-2;
		int maxY=ymax-2;
		Case[][] car=game.getGround().getBoard();
		
		
		switch(game.getLevel()) {
		case 1 :
		do {
		nbX = min + (int)(Math.random() * (maxX - min));
		nbY = min + (int)(Math.random() * (maxY - min));
		}while(!car[nbX][nbY].isEmpty());
		
		game.addSnake(nbX,nbY);
		break;
		case 2 :
			maxX-=2;
			maxY-=2;
			do {
				nbX = min + (int)(Math.random() * (maxX - min));
				nbY = min + (int)(Math.random() * (maxY - min));
				}while(!car[nbX][nbY].isEmpty());
				
				game.addSnake(nbX,nbY);
				
		break;
		case 3 :
			
			
			maxY=10;
			do {
				nbX = min + (int)(Math.random() * (maxX - min));
				nbY = min + (int)(Math.random() * (maxY - min));
				}while(!car[nbX][nbY].isEmpty());
				
				game.addSnake(nbX,nbY);
				break;
		case 4 :
			min=25;
			maxX=33;
			maxY=33;
			do {
				nbX = min + (int)(Math.random() * (maxX - min));
				nbY = min + (int)(Math.random() * (maxY - min));
				}while(!car[nbX][nbY].isEmpty());
				
				game.addSnake(nbX,nbY);
				
			break;
		case 5 :
			
			maxY=15;
			do {
				min=55;
				nbX = min + (int)(Math.random() * (maxX - min));
				min=2;
				nbY = min + (int)(Math.random() * (maxY - min));
				}while(!car[nbX][nbY].isEmpty());
				
				game.addSnake(nbX,nbY);
			break;
		}
			
	}
	
	public void UpdateFile() {
		try {
			FileWriter writer = new FileWriter(game.getFile());
			BufferedWriter bw = new BufferedWriter(writer);
			
			if(!game.getFile().exists()) {
				try {
					game.getFile().createNewFile();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
			}
			
			
			bw.write("Last score : "+game.getScore()+"\n");
			bw.write("Best score : "+game.getBestScore());
			bw.close();
			writer.close();
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
}
