package GUI;

import javax.swing.JLabel;

public class StartGame extends Thread {

	private JLabel startLabel;
	private Gaming gaming;
    private boolean running=true;
	
	
	/**
	 * @param startLabel
	 * @param gaming
	 */
	public StartGame(JLabel startLabel, Gaming gaming) {
		super();
		this.startLabel = startLabel;
		this.gaming = gaming;
	}



	public void run() {
		try {
			while(running) {
				startLabel.setText("CLIQUEZ SUR \"ENTREE\"");
				sleep(1000);
				startLabel.setText("");
				sleep(1000);
			}
			
			startLabel.setText("");
			gaming.resume();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}



	/**
	 * @return the startLabel
	 */
	public JLabel getStartLabel() {
		return startLabel;
	}



	/**
	 * @param startLabel the startLabel to set
	 */
	public void setStartLabel(JLabel startLabel) {
		this.startLabel = startLabel;
	}



	/**
	 * @return the gaming
	 */
	public Gaming getGaming() {
		return gaming;
	}



	/**
	 * @param gaming the gaming to set
	 */
	public void setGaming(Gaming gaming) {
		this.gaming = gaming;
	}



	/**
	 * @return the running
	 */
	public boolean isRunning() {
		return running;
	}



	/**
	 * @param running the running to set
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}
	
	
}
