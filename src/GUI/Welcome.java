package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.ArrayList;
import java.awt.CardLayout;
import java.awt.Color;

import javax.swing.SwingConstants;
import javax.swing.colorchooser.ColorSelectionModel;

import MODEL.*;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JColorChooser;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.GridLayout;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JTextArea;

public class Welcome {

	JFrame frame;
	private TitleAnimation animation;
	private JLabel ScoreLabel1;
	private JLabel ScoreLabel2;
	private JLabel ScoreLabel3;
	private JLabel ScoreLabel4;
	private JLabel ScoreLabel5;
	private Game game;
	private int levelSpeed;
	private int level;
	private Color[] color=new Color[3];
	private int selectColor=-1;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Welcome window = new Welcome();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Welcome() {
		Ground ground= new Ground(60,60);
		Snake snake=new Snake();
		color[0]=ground.getColorDef();
		color[1]=snake.getColor();
		color[2]=Color.RED;
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 813, 634);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JLayeredPane layeredPane = new JLayeredPane();
		frame.getContentPane().add(layeredPane, BorderLayout.CENTER);
		layeredPane.setLayout(new CardLayout(0, 0));
		
		JPanel welcomePane = new JPanel();
		welcomePane.setBackground(new Color(51, 153, 153));
		layeredPane.add(welcomePane, "name_283339924406000");
		welcomePane.setLayout(null);
		
		JPanel menuPane = new JPanel();
		menuPane.setBackground(new Color(51, 153, 153));
		menuPane.setBounds(196, 157, 415, 430);
		welcomePane.add(menuPane);
		
		JButton btnNewButton_1 = new JButton("SCORE");
		
		btnNewButton_1.setFont(new Font("Snap ITC", Font.BOLD, 24));
		
		JButton btnNewButton_2 = new JButton("SETTINGS");
		
		btnNewButton_2.setFont(new Font("Snap ITC", Font.PLAIN, 24));
		
		JButton btnNewButton_3 = new JButton("EXIT");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton_3.setFont(new Font("Snap ITC", Font.BOLD, 24));
		
		JButton btnNewButton_4 = new JButton("PLAY");
		
		btnNewButton_4.setFont(new Font("Snap ITC", Font.BOLD, 24));
		
		JButton btnNewButton_4_1 = new JButton("TUTORIAL");
		btnNewButton_4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_4_1.setFont(new Font("Snap ITC", Font.BOLD, 24));
		
		GroupLayout gl_menuPane = new GroupLayout(menuPane);
		gl_menuPane.setHorizontalGroup(
			gl_menuPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_menuPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_menuPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnNewButton_2, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
						.addComponent(btnNewButton_3, GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
						.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE)
						.addComponent(btnNewButton_4_1, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnNewButton_4, GroupLayout.DEFAULT_SIZE, 395, Short.MAX_VALUE))
					.addContainerGap())
		);
		gl_menuPane.setVerticalGroup(
			gl_menuPane.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_menuPane.createSequentialGroup()
					.addGap(37)
					.addComponent(btnNewButton_4, GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_4_1, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewButton_3, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		menuPane.setLayout(gl_menuPane);
		
		JPanel titlePane = new JPanel();
		titlePane.setBackground(new Color(112, 128, 144));
		titlePane.setForeground(Color.LIGHT_GRAY);
		titlePane.setBounds(196, 21, 415, 112);
		welcomePane.add(titlePane);
		titlePane.setLayout(new GridLayout(0, 5, 0, 0));
		
		JLabel sLabel = new JLabel("S");
		sLabel.setBackground(new Color(51, 153, 153));
		sLabel.setHorizontalAlignment(SwingConstants.CENTER);
		sLabel.setFont(new Font("Lucida Handwriting", Font.BOLD, 52));
		titlePane.add(sLabel);
		
		JLabel nLabel = new JLabel("N");
		nLabel.setBackground(new Color(51, 153, 153));
		nLabel.setHorizontalAlignment(SwingConstants.CENTER);
		nLabel.setFont(new Font("Lucida Handwriting", Font.BOLD, 52));
		titlePane.add(nLabel);
		
		JLabel aLabel = new JLabel("A");
		aLabel.setBackground(new Color(51, 153, 153));
		aLabel.setHorizontalAlignment(SwingConstants.CENTER);
		aLabel.setFont(new Font("Lucida Handwriting", Font.BOLD, 52));
		titlePane.add(aLabel);
		
		JLabel kLabel = new JLabel("K");
		kLabel.setBackground(new Color(51, 153, 153));
		kLabel.setHorizontalAlignment(SwingConstants.CENTER);
		kLabel.setFont(new Font("Lucida Handwriting", Font.BOLD, 52));
		titlePane.add(kLabel);
		
		JLabel eLabel = new JLabel("E");
		eLabel.setBackground(new Color(51, 153, 153));
		eLabel.setHorizontalAlignment(SwingConstants.CENTER);
		eLabel.setFont(new Font("Lucida Handwriting", Font.BOLD, 52));
		titlePane.add(eLabel);
		
		ArrayList<JLabel> liste = new ArrayList<JLabel>();
		liste.add(sLabel);
		liste.add(nLabel);
		liste.add(aLabel);
		liste.add(kLabel);
		liste.add(eLabel);
		
		JPanel levelPane = new JPanel();
		levelPane.setBackground(new Color(51, 153, 153));
		layeredPane.add(levelPane, "name_389837008834900");
		levelPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("CHOOSE A LEVEL");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Snap ITC", Font.BOLD, 24));
		lblNewLabel.setBounds(229, 76, 334, 79);
		levelPane.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 128, 128));
		panel.setBounds(268, 197, 255, 287);
		levelPane.add(panel);
		panel.setLayout(null);
		
		JButton startLevel1 = new JButton("LEVEL 1");
		startLevel1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				level=1;
				frame.dispose();
				Game game=new Game(level,levelSpeed,color);
				game.frmSnake.setVisible(true);
			}
		});
		startLevel1.setFont(new Font("Snap ITC", Font.BOLD, 18));
		startLevel1.setForeground(new Color(0, 0, 128));
		startLevel1.setBounds(10, 10, 235, 45);
		panel.add(startLevel1);
		
		JButton startLevel2 = new JButton("LEVEL 2");
		startLevel2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				level=2;
				frame.dispose();
				Game game=new Game(level,levelSpeed,color);
				game.frmSnake.setVisible(true);
			}
		});
		startLevel2.setFont(new Font("Snap ITC", Font.BOLD, 18));
		startLevel2.setForeground(new Color(0, 0, 128));
		startLevel2.setBounds(10, 65, 235, 45);
		panel.add(startLevel2);
		
		JButton startLevel3 = new JButton("LEVEL 3");
		startLevel3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				level=3;
				frame.dispose();
				Game game=new Game(level,levelSpeed,color);
				game.frmSnake.setVisible(true);
			}
		});
		startLevel3.setFont(new Font("Snap ITC", Font.BOLD, 18));
		startLevel3.setForeground(new Color(0, 0, 128));
		startLevel3.setBounds(10, 120, 235, 45);
		panel.add(startLevel3);
		
		JButton startLevel4 = new JButton("LEVEL 4");
		startLevel4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				level=4;
				frame.dispose();
				Game game=new Game(level,levelSpeed,color);
				game.frmSnake.setVisible(true);
			}
		});
		startLevel4.setFont(new Font("Snap ITC", Font.BOLD, 18));
		startLevel4.setForeground(new Color(0, 0, 128));
		startLevel4.setBounds(10, 175, 235, 45);
		panel.add(startLevel4);
		
		JButton startLevel5 = new JButton("LEVEL 5");
		startLevel5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				level=5;
				frame.dispose();
				Game game=new Game(level,levelSpeed,color);
				game.frmSnake.setVisible(true);
			}
		});
		startLevel5.setFont(new Font("Snap ITC", Font.BOLD, 18));
		startLevel5.setForeground(new Color(0, 0, 128));
		startLevel5.setBounds(10, 230, 235, 45);
		panel.add(startLevel5);
		
		JButton exitLevelButton = new JButton("EXIT");
		exitLevelButton.setFont(new Font("Snap ITC", Font.BOLD, 18));
		exitLevelButton.setBounds(10, 543, 173, 53);
		levelPane.add(exitLevelButton);
		
		JPanel scorePane = new JPanel();
		scorePane.setBackground(new Color(0, 139, 139));
		layeredPane.add(scorePane, "name_395812793480900");
		scorePane.setLayout(null);
		
		
		JLabel lblNewLabel_1 = new JLabel("YOUR BEST SCORES :");
		lblNewLabel_1.setFont(new Font("Snap ITC", Font.BOLD, 24));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(219, 78, 372, 71);
		scorePane.add(lblNewLabel_1);
		
		JPanel listScorePane = new JPanel();
		listScorePane.setBackground(new Color(0, 128, 128));
		listScorePane.setBounds(190, 182, 418, 359);
		scorePane.add(listScorePane);
		listScorePane.setLayout(null);
		
		ScoreLabel1 = new JLabel("LEVEL 1 : 0");
		ScoreLabel1.setHorizontalAlignment(SwingConstants.CENTER);
		ScoreLabel1.setFont(new Font("Snap ITC", Font.BOLD, 18));
		ScoreLabel1.setBounds(10, 10, 398, 59);
		listScorePane.add(ScoreLabel1);
		
		ScoreLabel2 = new JLabel("LEVEL 2 : 0");
		ScoreLabel2.setHorizontalAlignment(SwingConstants.CENTER);
		ScoreLabel2.setFont(new Font("Snap ITC", Font.BOLD, 18));
		ScoreLabel2.setBounds(10, 79, 398, 59);
		listScorePane.add(ScoreLabel2);
		
		ScoreLabel3 = new JLabel("LEVEL 3 : 0");
		ScoreLabel3.setHorizontalAlignment(SwingConstants.CENTER);
		ScoreLabel3.setFont(new Font("Snap ITC", Font.BOLD, 18));
		ScoreLabel3.setBounds(10, 148, 398, 59);
		listScorePane.add(ScoreLabel3);
		
		ScoreLabel4 = new JLabel("LEVEL 4 : 0");
		ScoreLabel4.setHorizontalAlignment(SwingConstants.CENTER);
		ScoreLabel4.setFont(new Font("Snap ITC", Font.BOLD, 18));
		ScoreLabel4.setBounds(10, 217, 398, 59);
		listScorePane.add(ScoreLabel4);
		
		ScoreLabel5 = new JLabel("LEVEL 5 : 0");
		ScoreLabel5.setHorizontalAlignment(SwingConstants.CENTER);
		ScoreLabel5.setFont(new Font("Snap ITC", Font.BOLD, 18));
		ScoreLabel5.setBounds(10, 286, 398, 59);
		listScorePane.add(ScoreLabel5);
		
		JButton exitScoreButton = new JButton("EXIT");
		exitScoreButton.setFont(new Font("Snap ITC", Font.BOLD, 18));
		exitScoreButton.setBounds(10, 543, 173, 53);
		scorePane.add(exitScoreButton);
		
		JPanel settingPane = new JPanel();
		settingPane.setBackground(new Color(0, 139, 139));
		layeredPane.add(settingPane, "name_396645735358800");
		settingPane.setLayout(null);
		
		JLabel lblNewLabel_7 = new JLabel("SPEED LEVEL : ");
		lblNewLabel_7.setFont(new Font("Snap ITC", Font.BOLD, 18));
		lblNewLabel_7.setBounds(10, 94, 270, 70);
		settingPane.add(lblNewLabel_7);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String level = (String)comboBox.getSelectedItem();
				
				switch (level) {
				case "EASY": 
					levelSpeed=1;
					break;
				case "NORMAL":
					levelSpeed=2;
					break;
				case "HARD": 
				    levelSpeed=3;
					break;
				case "ULTRA":
					levelSpeed=4;
					break;
				}
			}
		});
		comboBox.setFont(new Font("Tahoma", Font.BOLD, 18));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"EASY", "NORMAL", "HARD", "ULTRA"}));
		comboBox.setToolTipText("");
		comboBox.setBounds(290, 122, 239, 21);
		comboBox.setSelectedItem("NORMAL");
		settingPane.add(comboBox);
		
		JLabel lblNewLabel_7_1 = new JLabel("GROUND COLOR :");
		lblNewLabel_7_1.setFont(new Font("Snap ITC", Font.BOLD, 18));
		lblNewLabel_7_1.setBounds(10, 187, 270, 70);
		settingPane.add(lblNewLabel_7_1);
		
		JLabel lblNewLabel_7_1_1 = new JLabel("SNAKE COLOR :");
		lblNewLabel_7_1_1.setFont(new Font("Snap ITC", Font.BOLD, 18));
		lblNewLabel_7_1_1.setBounds(10, 256, 270, 70);
		settingPane.add(lblNewLabel_7_1_1);
		
		JLabel lblNewLabel_7_1_2 = new JLabel("FOOD COLOR :");
		lblNewLabel_7_1_2.setFont(new Font("Snap ITC", Font.BOLD, 18));
		lblNewLabel_7_1_2.setBounds(10, 326, 270, 70);
		settingPane.add(lblNewLabel_7_1_2);
		
		JButton colorButton1 = new JButton("CHOOSE COLOR");
		
		colorButton1.setFont(new Font("Tahoma", Font.BOLD, 18));
		colorButton1.setBounds(290, 211, 239, 31);
		settingPane.add(colorButton1);
		
		JButton colorButton2 = new JButton("CHOOSE COLOR");
		
		colorButton2.setFont(new Font("Tahoma", Font.BOLD, 18));
		colorButton2.setBounds(290, 276, 239, 31);
		settingPane.add(colorButton2);
		
		JButton colorButton3 = new JButton("CHOOSE COLOR");
		
		colorButton3.setFont(new Font("Tahoma", Font.BOLD, 18));
		colorButton3.setBounds(290, 343, 239, 31);
		settingPane.add(colorButton3);
		
		JButton exitSettingButton = new JButton("EXIT");
		
		exitSettingButton.setFont(new Font("Snap ITC", Font.BOLD, 18));
		exitSettingButton.setBounds(10, 534, 173, 53);
		settingPane.add(exitSettingButton);
		
		JPanel tutoPane = new JPanel();
		tutoPane.setBackground(new Color(0, 139, 139));
		layeredPane.add(tutoPane, "name_401642200233700");
		tutoPane.setLayout(null);
		
		JTextArea txtrWelcomeToSnake = new JTextArea();
		txtrWelcomeToSnake.setFont(new Font("Monospaced", Font.PLAIN, 18));
		txtrWelcomeToSnake.setText("WELCOME TO SNAKE GAME !!!\r\n\r\nGOAL :\r\nYou're going to control a SNAKE and must try to get the upper SCORE\r\nby making it eat a FOOD generated in a GROUND\r\nThe faster it is, the upper it is\r\n\r\nCOMMANDS :\r\nTURN : Arrows keys on Keyboard\r\nPAUSE/PLAY : Space\r\nEXIT : Back Space\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
		txtrWelcomeToSnake.setEditable(false);
		txtrWelcomeToSnake.setBounds(31, 95, 748, 423);
		tutoPane.add(txtrWelcomeToSnake);
		
		JButton exitTutoButton = new JButton("EXIT");
		exitTutoButton.setFont(new Font("Snap ITC", Font.BOLD, 18));
		exitTutoButton.setBounds(10, 543, 173, 53);
		tutoPane.add(exitTutoButton);
		
		readAllBestScore();
		
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				layeredPane.removeAll(); 
				layeredPane.add(levelPane);
				layeredPane.getIgnoreRepaint();
				layeredPane.revalidate();
				
			}
		});
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				layeredPane.removeAll(); 
				layeredPane.add(scorePane);
				layeredPane.getIgnoreRepaint();
				layeredPane.revalidate();
			}
		});
		
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				layeredPane.removeAll(); 
				layeredPane.add(settingPane);
				layeredPane.getIgnoreRepaint();
				layeredPane.revalidate();
			}
		});
		btnNewButton_4_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				layeredPane.removeAll(); 
				layeredPane.add(tutoPane);
				layeredPane.getIgnoreRepaint();
				layeredPane.revalidate();
			}
		});
		exitSettingButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				layeredPane.removeAll(); 
				layeredPane.add(welcomePane);
				layeredPane.getIgnoreRepaint();
				layeredPane.revalidate();
			}
		});
		exitScoreButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				layeredPane.removeAll(); 
				layeredPane.add(welcomePane);
				layeredPane.getIgnoreRepaint();
				layeredPane.revalidate();
			}
		});
		exitLevelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				layeredPane.removeAll(); 
				layeredPane.add(welcomePane);
				layeredPane.getIgnoreRepaint();
				layeredPane.revalidate();
			}
		});
		exitTutoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				layeredPane.removeAll(); 
				layeredPane.add(welcomePane);
				layeredPane.getIgnoreRepaint();
				layeredPane.revalidate();
			}
		});
		
		colorButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectColor=0;
				Color color2 = JColorChooser.showDialog(null, "Choose a color", Color.WHITE);
				colorButton1.setBackground(color2);
                color[selectColor]=color2;			
			}
		});
		colorButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectColor=1;
				Color color2 = JColorChooser.showDialog(null, "Choose a color", Color.WHITE);
				colorButton1.setBackground(color2);
                color[selectColor]=color2;
			}
		});
		colorButton3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectColor=2;
				Color color2 = JColorChooser.showDialog(null, "Choose a color", Color.WHITE);
				colorButton1.setBackground(color2);
                color[selectColor]=color2;
			}
		});
		
		animation = new TitleAnimation(liste);
		animation.start();
	}
	
	
	public void readAllBestScore(){
		for(int i=1;i<=5;i++) {
		 File file=new File("scoreLevel"+i+".txt");
		try {
			if(file.exists()) {
				String line;
				FileReader reader = new FileReader(file);
				BufferedReader br = new BufferedReader(reader);
				while((line=br.readLine())!=null) {
					if(line.startsWith("Best score : ")) {
						String bs = line.substring(13);
						int best=Integer.parseInt(bs);
						switch(i) {
						case 1 :
						ScoreLabel1.setText("LEVEL "+i+" : "+best);
						break;
						case 2 :
							ScoreLabel2.setText("LEVEL "+i+" : "+best);
							break;
						case 3 :
							ScoreLabel3.setText("LEVEL "+i+" : "+best);
							break;
						case 4 :
							ScoreLabel4.setText("LEVEL "+i+" : "+best);
							break;
						case 5 :
							ScoreLabel5.setText("LEVEL "+i+" : "+best);
							break;
						}
					}
					
			}
				br.close();
				reader.close();
			}
			
		
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
		
	}
}
